package com.citi.trading.pricing;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.logging.Logger;


/**
 * Client component for the pricing service. Holds a publish/subscribe registry
 * so that many subscribers can be notified of pricing data on a given stock,
 * based on a single request to the remote service. When configured as a Spring bean,
 * this component will make HTTP requests on a 15-second timer using 
 * Spring scheduling (if enabled).
 * 
 * Requires a property that provides the URL of the remote service. 
 * 
 * @author Will Provost
 */
public class Pricing {
	
    private static final Logger LOG =
            Logger.getLogger (Pricing.class.getName ());

	public static final int MAX_PERIODS_WE_CAN_FETCH = 120;
	public static final int SECONDS_PER_PERIOD = 15;
	public ArrayList<Subscriber> subscribers = new ArrayList<Subscriber>();
	
	public static PricePoint parsePricePoint(String CSV) {
		SimpleDateFormat parser = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");

		String[] fields = CSV.split(",");
		if (fields.length < 6) {
			throw new IllegalArgumentException
				("There must be at least 6 comma-separated fields: '" + CSV + "'");
		}
		
		try {
			Timestamp timestamp = new Timestamp(parser.parse(fields[0]).getTime());
			double open = Double.parseDouble(fields[1]);
			double high = Double.parseDouble(fields[2]);
			double low = Double.parseDouble(fields[3]);
			double close = Double.parseDouble(fields[4]);
			int volume = Integer.parseInt(fields[5]);
			
			return new PricePoint(timestamp, open, high, low, close, volume);
		} catch (Exception ex) {
			throw new RuntimeException("Couldn't parse timestamp.", ex);
		}
	}
	
	/**
	 * Requests data from the HTTP service and returns it to the caller.
	 */
	public void getPriceData() {
		try {
			//Data structures to store pricepoint information
			HashSet<String> stocksToPull = new HashSet<String>();
			HashMap<String, PricePoint> pricepoints = new HashMap<String, PricePoint>();
			for(Subscriber s: this.subscribers) {
				stocksToPull.add(s.getStock());
			}
			
			for(String s: stocksToPull) {
				LOG.config("Getting price point information for " + s);
				String requestURL = "http://will1.conygre.com:8081/prices/" + s + "?periods=1";
				BufferedReader in = new BufferedReader(new InputStreamReader
						(new URL(requestURL).openStream()));
				
				//First line are column names, ignore them
				in.readLine(); // header ... right? No way that could break ...
				
				String l = in.readLine();
				PricePoint p = Pricing.parsePricePoint(l);
				p.setStock(s);
				pricepoints.put(p.getStock(), p);
			}

			//Notify all subscribers
			for(Subscriber s: subscribers) {
				PricePoint p = pricepoints.get(s.getStock());
				s.update(p);
			}
		} catch (IOException ex) {
			throw new RuntimeException
					("Couldn't retrieve price .", ex);
		} catch(IllegalArgumentException ex) {
			throw new RuntimeException
					("Periods should between 1 and 30");
		}
	}
	
	//Subscribe
	public void addSubscriber(String stock) {
		Subscriber s = new Subscriber(stock);
		LOG.config("Subscriber added for stock " + stock);
		this.subscribers.add(s);
	}
	
	//Unsubscribe
	public void removeSubscriber(int id) {
		LOG.config("Subscriber removed with id " + id );
		this.subscribers.removeIf(subscriber -> subscriber.getId() == id);
	}
	
	/**
	 * Quick test of the component.
	 */
	public static void main(String[] args) throws Exception {
		Pricing pricing = new Pricing();

		pricing.addSubscriber("MRK");
		pricing.addSubscriber("AA");
		pricing.addSubscriber("ABC");
		pricing.getPriceData();
		Thread.sleep(16000);
		
		pricing.removeSubscriber(2);
		pricing.getPriceData();
		Thread.sleep(16000);
		
		pricing.addSubscriber("CITI");
		pricing.addSubscriber("JPM");
		pricing.getPriceData();
	}
}
