package com.citi.trading.pricing;

/**
 * Interface for the observer design pattern
 * @author Administrator
 *
 */
public interface ObserverInterface {
	public void update(PricePoint pricepoints);
}
