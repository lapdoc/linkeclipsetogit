package com.citi.trading.pricing;



public class Subscriber implements ObserverInterface {

	private static int id = 0;
	public String stock;
	public PricePoint price;
	public int identifier;
	
	public Subscriber(String stock) {
		this.stock = stock;
		this.identifier = id++;
	}
	
	@Override
	public void update(PricePoint pricepoint) {
		this.price = pricepoint;
		System.out.println(this.identifier + " " + this.price.toString());
	}
	
	public int getId() {
		return this.identifier;
	}
	
	public String getStock() {
		return this.stock;
	}
	
}
